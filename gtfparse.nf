#!/usr/bin/env nextflow

process gtfparse_tx_id_to_gene_name {
// Runs gtfparse to produce a transcript id to gene name map.
//
// input:
//   path gtf - Reference GTF
// output:
//   path("tx2gene.tsv") - transcript id to gene name map

// require:
//   params.gtfparse$gtfparse_tx_id_to_gene_name$gtf

  tag "${gtf}"
  label 'gtfparse_container'
  label 'gtfparse'

  input:
  path gtf

  output:
  path ("tx2gene.tsv"), emit: tx2gene

  script:
  """
#!/usr/bin/env python3

from gtfparse import read_gtf

df = read_gtf("${gtf}")

tx2gene = dict(zip(df['transcript_id'],df['gene_name']))

with open("tx2gene.tsv", 'w') as ofo:
    ofo.write("transcript_id\\tgene_name\\n")
    for tx, gene in tx2gene.items():
        if tx and gene:
            ofo.write(f"{tx}\\t{gene}\\n")
  """
}
